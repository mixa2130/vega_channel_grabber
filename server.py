import os
import logging
from sys import stdout

import aiohttp
from dotenv import load_dotenv
from telethon import TelegramClient, events
from handler import DataSync

# Logging
logger = logging.getLogger('server')
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(stdout)
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')

handler.setFormatter(formatter)
logger.addHandler(handler)

# Configuration
load_dotenv()
api_id: int = int(os.getenv('API_ID'))
api_hash: str = os.getenv('API_HASH')

_source_channels: str = os.getenv('CHANNELS')  # "test_channel1_id, test_channel2_id..."
channels: list = _source_channels.split(',')
for index, _raw_channel in enumerate(channels):
    channels[index] = int(_raw_channel)

# REST API
REST_URL: str = os.getenv('REST_URL')
REST_ENDPOINT: str = os.getenv('REST_ENDPOINT')
REST_TOKEN: str = os.getenv('REST_TOKEN')  # auth token, which is transmitted as a json field in an output structure
REST_URI: str = f"{REST_URL}{REST_ENDPOINT}"

client = TelegramClient('grabber', api_id, api_hash)


async def rest_api(json_data: dict):
    """
    Responsible for sending data to the server using REST API.
    REST_URI is built on REST_URL and REST_ENDPOINT from .env.
    """
    json_data['REST_TOKEN'] = REST_TOKEN

    async with aiohttp.ClientSession() as session:
        try:
            async with session.post(REST_URI, json=json_data,
                                    headers={'Content-Encoding': 'UTF-8'}) as response:
                status = response.status
                logger.debug('Статус отправки: %s', status)

                if status // 100 in (4, 5):
                    logger.error('HTTP error: %s - %s', status, response.reason)

        except Exception as exc:
            logger.error(repr(exc))


@client.on(events.NewMessage(chats=channels))
@client.on(events.MessageEdited(chats=channels))
async def grab_msg_from_channel(event):
    data = DataSync(event.message)
    json_data: dict = data.to_dict()

    await rest_api(json_data)


if __name__ == '__main__':
    client.start()
    client.run_until_disconnected()
