import unittest
from unittest.mock import patch
import telethon.tl.types as typed
import datetime

from handler import DataSync


class TestDataSync(unittest.TestCase):
    def test_DataSync_init_text(self):
        text_msg = typed.Message(id=9,
                                 peer_id=typed.PeerChannel(channel_id=1129510527),
                                 date=datetime.datetime(2021, 5, 17, 12, 45, 11, tzinfo=datetime.timezone.utc),
                                 message='Text_check',
                                 media=None,
                                 reply_markup=None, entities=[], views=1, forwards=0, replies=None, edit_date=None,
                                 post_author=None, grouped_id=None, restriction_reason=[], ttl_period=None)
        data = DataSync(text_msg)
        correct_output: dict = {'chat': {'id': 1129510527, 'type': 'channel'}, 'edited': False, 'message_id': 9,
                                'text': 'Text_check', 'date': 1621255511.0}

        self.assertEqual(data.to_dict(), correct_output)

    def test_photo_msg(self):
        photo_msg = typed.Message(id=30, peer_id=typed.PeerChannel(channel_id=1129510527),
                                  date=datetime.datetime(2021, 5, 19, 10, 51, 1, tzinfo=datetime.timezone.utc),
                                  message='Photo_check', media_unread=False, silent=False,
                                  post=True, edit_hide=False,
                                  pinned=False, from_id=None, fwd_from=None,
                                  reply_to=None,
                                  media=typed.MessageMediaDocument(
                                      document=typed.Document(id=5271929908954664567, access_hash=7266315819343623049,
                                                              file_reference=b'\xb9',
                                                              date=datetime.datetime(2021, 5, 19, 10, 51, 1,
                                                                                     tzinfo=datetime.timezone.utc),
                                                              mime_type='image/jpeg', size=127976, dc_id=2,
                                                              attributes=[
                                                                  typed.DocumentAttributeImageSize(w=1280, h=1248),
                                                                  typed.DocumentAttributeFilename(file_name='3.jpg')],
                                                              )),
                                  reply_markup=None, forwards=0, replies=None, edit_date=None,
                                  post_author=None)

        data = DataSync(photo_msg)
        correct_output: dict = {'chat': {'id': 1129510527, 'type': 'channel'}, 'edited': False, 'message_id': 30,
                                'text': 'Photo_check', 'date': 1621421461.0,
                                'file': {'file_id': 5271929908954664567, 'file_type': 'photo', 'file_name': '3.jpg',
                                         'file_size': 127976}}

        self.assertEqual(data.to_dict(), correct_output)

    def test_doc_msg(self):
        doc_msg = typed.Message(id=33, peer_id=typed.PeerChannel(channel_id=1129510527),
                                date=datetime.datetime(2021, 5, 19, 10, 56, 35, tzinfo=datetime.timezone.utc),
                                message='Document_check', edit_hide=False, pinned=False, from_id=None,
                                fwd_from=None, via_bot_id=None,
                                media=typed.MessageMediaDocument(
                                    document=typed.Document(id=5271929908954664573, access_hash=-3810967599545845773,
                                                            file_reference=b'02',
                                                            date=datetime.datetime(2021, 5, 19, 10, 56, 34,
                                                                                   tzinfo=datetime.timezone.utc),
                                                            mime_type='application/pdf', size=139226, dc_id=2,
                                                            attributes=[typed.DocumentAttributeFilename(
                                                                file_name='Функциональный_анализ_Типовой_расчет.pdf')]
                                                            )
                                ), forwards=0, replies=None, edit_date=None,
                                post_author=None)

        data = DataSync(doc_msg)
        correct_output: dict = {'chat': {'id': 1129510527, 'type': 'channel'}, 'edited': False, 'message_id': 33,
                                'text': 'Document_check', 'date': 1621421795.0,
                                'file': {'file_id': 5271929908954664573, 'file_type': 'document',
                                         'file_name': 'Функциональный_анализ_Типовой_расчет.pdf', 'file_size': 139226}}

        self.assertEqual(data.to_dict(), correct_output)

    def test_edit_msg(self):
        text_msg = typed.Message(id=84, peer_id=typed.PeerChannel(channel_id=1129510527),
                                 date=datetime.datetime(2021, 5, 21, 14, 12, 30, tzinfo=datetime.timezone.utc),
                                 message='ok?maybe not', out=True, mentioned=False, media_unread=False,
                                 silent=False, post=True, from_scheduled=False, legacy=False, edit_hide=False,
                                 pinned=False, from_id=None, fwd_from=None, via_bot_id=None, reply_to=None,
                                 media=None, reply_markup=None, entities=[], views=1, forwards=0, replies=None,
                                 edit_date=datetime.datetime(2021, 5, 21, 14, 29, 31,
                                                             tzinfo=datetime.timezone.utc), post_author=None,
                                 grouped_id=None, restriction_reason=[], ttl_period=None)
        text_data = DataSync(text_msg)

        correct_output: dict = {'chat': {'id': 1129510527, 'type': 'channel'}, 'edited': True, 'message_id': 84,
                                'text': 'ok?maybe not',
                                'date': 1621607371.0}
        self.assertEqual(text_data.to_dict(), correct_output)

        doc_msg = typed.Message(id=33, peer_id=typed.PeerChannel(channel_id=1129510527),
                                date=datetime.datetime(2021, 5, 19, 10, 56, 35, tzinfo=datetime.timezone.utc),
                                message='Document_check', edit_hide=False, pinned=False, from_id=None,
                                fwd_from=None, via_bot_id=None,
                                media=typed.MessageMediaDocument(
                                    document=typed.Document(id=5271929908954664573, access_hash=-3810967599545845773,
                                                            file_reference=b'02',
                                                            date=datetime.datetime(2021, 5, 19, 10, 56, 34,
                                                                                   tzinfo=datetime.timezone.utc),
                                                            mime_type='application/pdf', size=139226, dc_id=2,
                                                            attributes=[typed.DocumentAttributeFilename(
                                                                file_name='Функциональный_анализ_Типовой_расчет.pdf')]
                                                            )
                                ), forwards=0, replies=None, edit_date=datetime.datetime(2021, 5, 21, 14, 29, 31,
                                                                                         tzinfo=datetime.timezone.utc),
                                post_author=None)

        doc_data = DataSync(doc_msg)
        correct_output: dict = {'chat': {'id': 1129510527, 'type': 'channel'}, 'edited': True, 'message_id': 33,
                                'text': 'Document_check', 'date': 1621607371.0,
                                'file': {'file_id': 5271929908954664573, 'file_type': 'document',
                                         'file_name': 'Функциональный_анализ_Типовой_расчет.pdf', 'file_size': 139226}}

        self.assertEqual(doc_data.to_dict(), correct_output)

        photo_msg = typed.Message(id=30, peer_id=typed.PeerChannel(channel_id=1129510527),
                                  date=datetime.datetime(2021, 5, 19, 10, 51, 1, tzinfo=datetime.timezone.utc),
                                  message='Photo_check', media_unread=False, silent=False,
                                  post=True, edit_hide=False,
                                  pinned=False, from_id=None, fwd_from=None,
                                  reply_to=None,
                                  media=typed.MessageMediaDocument(
                                      document=typed.Document(id=527192990895466456, access_hash=7266315819343623049,
                                                              file_reference=b'\xb9',
                                                              date=datetime.datetime(2021, 5, 19, 10, 51, 1,
                                                                                     tzinfo=datetime.timezone.utc),
                                                              mime_type='image/jpeg', size=127976, dc_id=2,
                                                              attributes=[
                                                                  typed.DocumentAttributeImageSize(w=1280, h=1248),
                                                                  typed.DocumentAttributeFilename(file_name='3.jpg')],
                                                              )),
                                  reply_markup=None, forwards=0, replies=None,
                                  edit_date=datetime.datetime(2021, 5, 21, 14, 29, 31,
                                                              tzinfo=datetime.timezone.utc),
                                  post_author=None)

        img_data = DataSync(photo_msg)
        correct_output: dict = {'chat': {'id': 1129510527, 'type': 'channel'}, 'edited': True, 'message_id': 30,
                                'text': 'Photo_check', 'date': 1621607371.0,
                                'file': {'file_id': 527192990895466456, 'file_type': 'photo', 'file_name': '3.jpg',
                                         'file_size': 127976}}

        self.assertEqual(img_data.to_dict(), correct_output)

    def test_error_cases(self):
        doc_msg = typed.Message(id=1, peer_id=typed.PeerChannel(channel_id=1129510527),
                                date=datetime.datetime(2021, 5, 19, 10, 56, 35, tzinfo=datetime.timezone.utc),
                                message='Document_check', edit_hide=False, pinned=False, from_id=None,
                                fwd_from=None, via_bot_id=None,
                                media=typed.MessageMediaDocument(
                                    document=typed.Document(id=5271929908954664573, access_hash=-3810967599545845773,
                                                            file_reference=b'02',
                                                            date=datetime.datetime(2021, 5, 19, 10, 56, 34,
                                                                                   tzinfo=datetime.timezone.utc),
                                                            mime_type='application/pdf', size=139226, dc_id=2,
                                                            attributes=[typed.DocumentAttributeFilename(
                                                                file_name='Функциональный_анализ_Типовой_расчет.pdf')]
                                                            )
                                ), forwards=0, replies=None, edit_date=None,
                                post_author=None)

        correct_output: dict = {"channel_id": 1129510527,
                                "error": "Message parser error, please check logs"}

        with patch('handler.DataSync._file_info_handler', side_effect=AttributeError):
            file_info_err = DataSync(doc_msg)

            self.assertEqual(file_info_err.to_dict(), correct_output)


if __name__ == '__main__':
    unittest.main()
