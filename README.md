# vega_channel_grabber

Telegram приложение для обработки данных, публикуемых в интересных заказчику telegram каналах

## How to install

### Подготовка приложения

1) Войти в [telegram core](https://my.telegram.org/)
2) Посетить [API development tools](https://my.telegram.org/apps)
3) Полученные `api_id` и `api_hash` занести в файл конфигурации

### Конфигурация

Приложение конфигурируется с помощью файла переменных окружений `.env` в рабочей директории.

```env
# API параметры
# api_id и api_hash из https://my.telegram.org/apps
API_ID=...
API_HASH=...

# ID интересующих telegram каналы, перечисленные через запятую
CHANNELS="15679876, 273273832"

# Параметры REST-api для отправки событий
REST_URL=https://example.com
REST_ENDPOINT=/path/to/api
# Токен для аутентификации
REST_TOKEN="abrakadabra"
```

## REST

Запрос к серверу формируется на основе данных, указанных в файле `.env`:

`{REST_URL}{REST_ENDPOINT}`

`REST_TOKEN` передаётся json полем вместе с данными

Пример HTTP запроса со стороны клиента:

```http request
POST /endpoint HTTP/1.1
Host: example.com
Content-Encoding: UTF-8
Content-Type: application/json
Content-Length: 215

{
  "chat": {
    "id": 1001162310979,
    "type": "channel"
  },
  "edited": false,
  "pinned_message": false,
  "message_id": 290,
  "author_nickname": "Gree",
  "text": "Умный текст",
  "date": 1616170950,
  "REST_TOKEN": "abrakadabra"
}
```

## Output data format

* `chat` — `dict` - описание канала, в который пришло сообщение:
    * `id` — `signed int` - идентификатор канала;
    * `type` — `str` - тип — `channel`;
* `edited` — `bool` - признак редактирования сообщения;
* `message_id` — `int` - идентификатор сообщения. Не меняется в случае редактирования или закрепления;
* `author_nickname` — `str` - ***optional*** - имя автора сообщения;
* `text` — `str` - текст сообщения;
* `file` — `dict` - ***optional*** - прикреплённый файл:
    * `file_id` — `str` - идентификатор файла;
    * `file_type` — `str` - тип файла (document, photo);
    * `file_size` — `int` - размер файла;
* `date` — `float` - время отправки сообщения в виде unix timestamp;
* `REST_TOKEN` — `str` - идентификационный токен.

### Ошибка при разборе сообщения

В случае ошибки — сервер получит соответствующее сообщение:

```json
{
  "channel_id": -1001162310979,
  "error": "Message parser error, please check logs",
  "REST_TOKEN": "abrakadabra"
}
```

Код ошибки, а также её описание — будут занесены в stdout logger.

