"""
Output format:

* `chat` — `dict` - channel description:
    * `id` — `signed int` - channel id;
    * `type` — `str` - type — `channel`;

* `edited` — `bool` - was the post edited / stays false if the edited message was pinned;
* `message_id` — `int` - tg post id. In the case of editing/pinning - the id of the message,
        not the operation itself. In other words - id does not change when editing or pinning;
* `author_nickname` — `str` - ***optional*** - post creator nickname (post may have no author);
* `text` — `str` - text in the post;
* `date` — `float` - date in numeric format (unix timestamp);

* `file` — `dict` - ***optional*** - file description:
    * `file_id` — `str` - file if in tg database;
    * `file_type` — `str` - type: document, photo;
    * `file_size` — `int` - file size;
"""
from datetime import datetime
import logging
import telethon.tl.types as typed


class DataSync:
    """TG channel posts processing"""

    def __init__(self, message: typed.Message):
        self.logger = logging.getLogger('server.handler.DataSync')
        self.error_happened: bool = False

        try:
            self.chat: dict = {
                'id': int(message.peer_id.channel_id),
                'type': 'channel'
            }

            if message.edit_date is None:
                self.edited: bool = False
                date: datetime = message.date
            else:
                self.edited: bool = True
                date: datetime = message.edit_date

            self.message_id: int = message.id
            self.text: str = message.message
            self.date: float = date.timestamp()

            # Optional params
            self.author_nickname: str = message.post_author

            self.file = {}
            if message.media is not None:
                # File description
                file_desc = self._file_info_handler(message.media)

                if len(file_desc) != 0:
                    self.file: dict = file_desc
        except Exception as exc:
            self.error_happened: bool = True
            self.logger.error('DataSync initialization error: %s. Channel id: %s',
                              repr(exc), self.chat['id'])

    @staticmethod
    def _file_info_handler(media: typed.MessageMediaDocument) -> dict:
        """
        Gets file info from message.

        :param media: channel post description
        :return: file description: {'file_id':,'file_type':, 'file_name':, 'file_size':}
        """
        if isinstance(media, typed.MessageMediaDocument):
            document = media.document

            _type: str = document.mime_type
            if _type.find('application/') != -1:
                file_type = 'document'
            elif _type.find('image/') != -1:
                file_type = 'photo'
            else:
                return {}

            file_name = ''
            for attr in document.attributes:
                try:
                    file_name = attr.file_name
                except AttributeError:
                    continue

            return {'file_id': document.id,
                    'file_type': file_type,
                    'file_name': file_name,
                    'file_size': document.size}

        return {}

    def to_dict(self) -> dict:
        """
        Generates a dictionary from class instance data - if there were no errors during initialization.

        :returns: no errors during init:
                     {'chat': ...,
                      'edited': ...,
                      'message_id': ...,
                      'text': ...,
                      'date': ...,
                      'file': .., # optional param
                      'author_nickname': .. #optional param
                      }
                  else:
                     {"error": "Message parser error, please check logs"}
        """
        if self.error_happened:
            return {"channel_id": self.chat['id'],
                    "error": "Message parser error, please check logs"}

        data_sync = {'chat': self.chat,
                     'edited': self.edited,
                     'message_id': self.message_id,  # doesn't change after message editing
                     'text': self.text,
                     'date': self.date
                     }

        # Optional parameters
        if self.author_nickname is not None:
            data_sync['author_nickname'] = self.author_nickname

        if len(self.file) > 0:
            data_sync['file'] = self.file

        return data_sync
